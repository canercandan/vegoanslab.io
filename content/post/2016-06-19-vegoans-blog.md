+++
title = "The Vegoans Community Blog"
date = "2016-06-19T14:50:46+02:00"
tags = ["blog"]
categories = ["tools"]
menu = ""
banner = "banners/community-blog.jpg"
authors = ["Geraldine Starke"]
+++

Here is a blog for the community around Vegoa:

We want it to be a place to share experiences, thoughts and informative content. No need to be official member to be part of this community and share with us. We believe everyone’s input is an enrichment and that will make the difference. It will also clearly be a great way for the project to spread and let people hear about us, our beautiful project and our objectives.

On the more technical aspect, the blog is hosted on a git repository (don’t worry an article about git functioning will soon be published), meaning every modifications is tracked, the content is public and transparent and there is no risk of deleting it and loosing any content. It’s a great tool to show we are confident about our project and the informations we share.

Every time someone writes an article it is submitted through a pull request on the git repository. There will be a group of members (any member that wishes to be part of) that will then accept this request or ask for modifications for example. Everyone that writes something will go through this process, even the members themselves. And all articles that are declined or ask to be modified will still be in the history and searchable.

A blog manifest is defined (and can be improved) to ensure some simple rules for writing an article (like not allowing insults, aggressivity or encouraging to write in a readable way, taking care of the spelling and so on).

The articles can be written in any language. We will have a great international content and it will just represent the diversity and human wealth there is in the Vegoa community. The articles can be ordered by categories or tags so it helps readers to find content easily.

In a next blog post we will explain in details how you can write a blog article using Git and the Gitlab interface.
