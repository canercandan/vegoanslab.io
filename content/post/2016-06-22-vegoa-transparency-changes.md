+++
title = "For true transparency in Vegoa"
date = "2016-06-22T15:50:46+02:00"
tags = ["transparency"]
categories = ["tools", "transparency"]
menu = ""
banner = "banners/transparency.jpg"
authors = ["Geraldine Starke"]
+++

Hello vegan friends,

I have listed here the new tools we made available to help Vegoa be fully transparent, ease communication and empower people. As well as a list of things that have to be changed in order to achieve full transparency, avoid any ownership and be a no-trust project (meaning as everything is clear there is no NEED for trust). And I personally think that we can not ask people to trust, we should show them we have nothing to hide, that everyone has a voice, and that the “power” is decentralized.

In such projects that gather so many people together it’s important to feel secure, to feel being a part of it and to provide tools that allow full transparency where everything is historized and nothing can be deleted. This will make people responsible and help avoid conflicts.

These tools will also encourage open-knowledge as people will be able to share their experience as well as detail Vegoa’s philosophy and concept. Altogether we are forming the Vegoa community and I believe this is what will make the project succeed.

## Tools that we made available now:

* **Slack Vegan Hills and Vegoa archives**: The slack group Vegan Hills and Vegoa are re-opened as archives, only to consult. The discussions can continue on one of the next tools.
* **Vegoans blog**:  A blog for the Vegoa community for anybody wishing to write about Vegoa. The blog will be using git repository allowing to historize everything, article will be added through push requests. Nothing can be deleted. The comments to the blog posts will only possible through the forum. It is an important tool to spread knowledge and empower people.
* **Vegoans forum**: A forum, open to anyone, is a good additional tool to be able to discuss, debate and ask questions. The forum is public.
* **Old Vegoa website archive**: The old Vegoa website is cloned on a new platform allowing to connect it to a git repository. This will allow to track every single change that has been made and to keep a history but it will mostly serve as archive as it was deleted without the agreement of the members. Again here, through this new tool, nothing is deletable.
* **Accountability Git Repository** (*Project blocked currently because of tool ownership being currently still centralised*): Every new version of the bank account and the gnucash file is saved on a git repository to have history and follow up. Anyone can make a pull request to update the accountability (GnuCash doc) that has to be accepted to be saved. This will share the responsibility about the accountability and make it transparent. Although the files that are shared could be falsifiable (that’s why we need to realize the steps listed under “Changes needed to achieve full transparency and decentralization”), it’s a good start and will allow to keep track.
* **Vegoa Manifest Git Repository**: every version of the manifest will be added to the git repository for approval by the members and history. Everything is public and not deletable.
* **Communication organization Git Repository** (*Project blocked currently because of tool ownership being currently still centralised*): a git repository for the communication organization with details on members, votes, decisions, costs and so on. Helps keep track and make things transparent. It’s important to do it for every organization.
* **Association Git Repository** (*under construction  but will not be complete because of tool ownership being currently still centralised*): a git repository to host all the official association documents, association minutes, assembly and votes. Again for history and transparency.

## Changes needed to achieve full transparency and decentralization:

* **Domain name vegoa.org**: belongs, since recently, to Vegoa association but only Fabio Victoriano has access and control over it. Other members should be added for a start. Another solution will be proposed later on to allow to be fully decentralized and transparent.
* **Create open organization for Bank account**: The association’s bank account is only handled and accessed by Tiago Pita and Erwan Le Rousseau at the moment. It is needed to create an open organization where any member that joins get access to the bank account online. This will again ensure full transparency.
* **Transparency about bank account**: Erwan/Tiago should explain (on a blog article so there is a footprint) how does the bank account works, what are the access requested, who can make payments/transfers/withdraws and all that there is to know on this matter. This way we can share responsibilities and if any member wishes to join the bank account organization they know what to expect.
* **Upload bank statements directly to Git repository**: Connect an API that has access to the bank account and saves everyday the bank statement directly into the git repository, so no human interaction is needed and it’s tracked. We have to research to see if it is possible, and it depends if the bank gives API rights on it.
* **Create organizations repository**: the members of current organizations (communication, camping, rice farm, broccoli canteen…) and the new organizations that will be created, must use a git repository for their organizations from day 1, to include a manifest/rules, members, and all the details needed so there is a history available to everyone to see.
* **FB groups/pages**: FB groups can be deleted by it’s creator (owner). Even if there are several admins, they don’t have the same rights. Nobody can stop the owner from deleting the group and its discussions and files. This means that FB groups can not be seen as official and most importantly can not be used for important decision making, polls, votes and official content. They should only be used as a publishing platform to **share** the content/information that have been created/decided in the decentralized and fully transparent tools available for Vegoa (the vegoans blog, the vegoa blog, the git repositories and the website as soon as the domain name is owned by the association).
* **Association new elections**: Maybe it is needed to make new elections if the members wish so, to define the members that should be part of presidency, treasury and assembly of the association as it could not be done before. This time the vote will be official and tracked on Git Repository. We could decide to make new elections every 6 months (or more/less) for example to distribute this roles and responsibilities.
* **Article about association role**: We should clearly define, altogether, what is the association and its members role and take into account the legal aspect to do so. After that an article explaining all this should be written and published on the Vegoa official blog (the new one hosted on git for full transparency) so it’s cast in stone and clear to everyone.

These are the points we can think of in the moment but we would be happy for your contribution if you think of other problems and solutions.

All the above mentioned points will be detailed, one by one, in new blog articles so you can know how to use them and what their purposes are.

I truly believe it is possible to create a fully transparent, decentralized project where we are not risking to fall back to society’s traps again. Will you help us achieve that and be part of a community that respects individual freedom and empowers its members?
