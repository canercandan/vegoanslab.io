# Vegoans Community Website

You can visit the official website of the Vegoans Community at [vegoans.org](https://vegoans.org).

## Content Generator

The website uses the Hugo Static Code Generator and is currently based on the Hugo Icarus theme by [Digital Craftsman](http://github.com/digitalcraftsman/hugo-icarus-theme).

If you have Hugo installed, simply run `hugo serve` in your command line and preview the build in your browser.

## Contributing

Please consider reading the [contributing guide](CONTRIBUTING.md) if you want to contribute to the project or create new content.

## Code of Conduct

The community is one of the best features of the Vegoans Community, and we want to ensure it remains welcoming and safe for everyone.
We have adopted the Contributor Covenant for all projects in the @Vegoans Gitlab group, the discussion forum, chat rooms, mailing list, social media tools, meetups and any other public event related to Vegoa.
This code of conduct outlines the expectations for all community members, as well as steps to report unacceptable behavior.
We are committed to providing a welcoming and inspiring community for all and expect our code of conduct to be honored.

* **The Code of Conduct is available [here](CODE_OF_CONDUCT.md).**
